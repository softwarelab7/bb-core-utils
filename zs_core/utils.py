import bisect
import datetime
import operator
import random
import re
import six
from decimal import Decimal
from django.utils import timezone
from django.utils.timezone import get_fixed_timezone, utc
from math import radians, sin, cos, sqrt, asin


DATE_WITH_TZ_RE = re.compile(
    r'(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})'
    r'(?P<tzinfo>Z|[+-]\d{2}(?::?\d{2})?)?$'
)

def fix_date(datetime_instance):
    """
    HACK: Right now app has bug that sends in next year if its last week of the year.
    """
    if datetime_instance is not None:
        now = datetime.datetime.now(datetime_instance.tzinfo)
        if datetime_instance > now + datetime.timedelta(days=240):
            datetime_instance_year = datetime_instance.year
            datetime_instance = datetime_instance.replace(year=datetime_instance_year-1)
    return datetime_instance


def get_form_tzinfo(dt):
    # this is for input form where date and time are separate to get
    # the proper tzinfo for the datetime when those two are combined
    current_tz = timezone.get_current_timezone()
    tzinfo = current_tz.normalize(dt.replace(tzinfo=current_tz)).tzinfo
    return tzinfo


def get_normalized_tz(dt):
    # current_tz gets the timezone that is currently used in django project
    # astimezone gets the time in that timezone.
    # normalize adjusts for daylight savings, since this is only known at
    # the exact point in time being looked at
    current_tz = timezone.get_current_timezone()
    normalized = current_tz.normalize(dt.astimezone(current_tz))
    return normalized


def parse_date_with_tz(datestring):
    """Adapted from: https://github.com/django/django/blob/master/django/utils/dateparse.py
    because Py<3.2 is inconsistent with %z datetime format support.
    """
    match = DATE_WITH_TZ_RE.match(datestring)
    if match:
        kw = match.groupdict()
        tzinfo = kw.pop('tzinfo')
        if tzinfo == 'Z':
            tzinfo = utc
        elif tzinfo is not None:
            offset_mins = int(tzinfo[-2:]) if len(tzinfo) > 3 else 0
            offset = 60 * int(tzinfo[1:3]) + offset_mins
            if tzinfo[0] == '-':
                offset = -offset
            tzinfo = get_fixed_timezone(offset)
        kw = {k: int(v) for k, v in six.iteritems(kw) if v is not None}
        kw['tzinfo'] = tzinfo
        return datetime.datetime(**kw)


def parse_tz_to_offset_mins(tzinfo):
    if tzinfo == 'Z':
        offset = 0
    elif tzinfo is not None:
        offset_mins = int(tzinfo[-2:]) if len(tzinfo) > 3 else 0
        offset = 60 * int(tzinfo[1:3]) + offset_mins
        if tzinfo[0] == '-':
            offset = -offset
    return offset


# accumulate isn't available in python 2.7, so make own accumulate
def accumulate(iterable, func=operator.add):
    """
    'Return running totals'
    accumulate([1,2,3,4,5]) --> 1 3 6 10 15
    accumulate([1,2,3,4,5], operator.mul) --> 1 2 6 24 120
    """
    it = iter(iterable)
    total = next(it)
    yield total
    for element in it:
        total = func(total, element)
        yield total


def get_weighted_choice(weighted_choices):
    # from python random library docs
    # https://docs.python.org/3/library/random.html
    choices, weights, _ = zip(*weighted_choices)
    cumdist = list(accumulate(weights))
    x = random.random() * cumdist[-1]
    return choices[bisect.bisect(cumdist, x)]
