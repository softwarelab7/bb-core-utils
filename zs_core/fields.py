from django.forms import ModelChoiceField, ModelMultipleChoiceField


class UserFullNameLabelMixin(object):
    def label_from_instance(self, obj):
        if obj.get_full_name():
            return u'{} <{}>'.format(obj.get_full_name(), obj.email)
        elif obj.email:
            return u'<{}>'.format(obj.email)
        else:
            return obj.username


class UserModelChoiceField(UserFullNameLabelMixin, ModelChoiceField):
    pass


class UserModelMultipleChoiceField(UserFullNameLabelMixin, ModelMultipleChoiceField):
    pass
