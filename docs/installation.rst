============
Installation
============

At the command line::

    $ easy_install zs-core-utils

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv zs-core-utils
    $ pip install zs-core-utils
